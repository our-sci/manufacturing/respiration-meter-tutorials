## Documentation template

This MkDocs template was forked from the Our Sci template project. You can use it to create a static HTML webpage of whatever documentation you save in this repository.  It creates a nice indexed structure like this https://www.mkdocs.org/user-guide/writing-your-docs/.

## HTML webpage information

- site_name: Our-Sci Respiration Meter User Tutorials
- site_url: https://our-sci.gitlab.io/manufacturing/respiration-meter-tutorials/

Now you can add markdown files to your repository and the index will match the folder structure of your files!

## Learn more about mkdocs generally

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

## Tips for using MkDocs

See a brief written tutorial from Nat on how to get started with MkDocs [here](https://our-sci.gitlab.io/mkdocs/tips_for_contributing/).

