# Overview

Soil mineralizable carbon (aka soil respiration) is a measurement that consists of rewetting air-dried soil, a short-term incubation (1-3 days), and then measuring the carbon emitted by soil micro-organisms during the incubation period. Due to its value as a soil health indicator, it is a recommended component of soil health assessments by both the [USDA](https://www.nrcs.usda.gov/conservation-basics/natural-resource-concerns/soils/soil-health/soil-health-assessment) and the [Soil Health Institute](https://soilhealthinstitute.org/our-work/initiatives/measurements/).


### Measurement benefits
* Strong indicator of soil health
* affects the carbon and nutrient cycling capacity of the soil
* relates strongly to microbial activity


### Kit components
* Motor automates CO2 measurments, ensuring accurate CO2 concentration measurements.
* Integrated scale and water pump make measurement set-up simple and consistent. 
* Kit includes an incubator capable of maintaining temperatures within +/- 10C of the rooms ambient temeprature.
* 35 syringes, enough to run batches of 35 samples.

## What can it do
### Motivation behind the SAVR Kit
We became interested in new methods/technologies to scale up soil respiration measurements while operating the Bionutrient Institute lab from 2018-2022. Our goal was to better understand the links between crop management, soil health and nutritional outcomes through a large-scale observational study. Unfortunately, the cost of soil respiration via common approaches such as commercial labs ($25-30 / sample) or Solvita kits (up-front costs of more than $1000 and consumable costs of $15+ per sample) were too high when collecting hundreds to thousands of samples. 

We decided to develop our own soil respiration kit that would have lower per samples costs, enable higher throughput, but still be accurate enough to output comparable data across time and space.

### Our Design
[SPECIFICS COMING SOON]

## Our Mission
We strive to support the development of research capacity in communities through software, hardware, and training. Specifically, we want to help turn citizens into researchers, researchers into community leaders, and communities into solvers.

With our low-cost tools, we aim to:

* Help communities disrupt the systems that they think need disruption
* Improve data accessibility across a diverse set of users
* Increase the scientific rigor of community-based projects and decision-making
* Foster a network of communication, trust, and independence
* Be transparent and open source in everything we do
