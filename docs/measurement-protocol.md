## Standard Soil Respiration Measurement Protocol
### SAVR Kit Software
The SAVR Kit connects to the SurveyStack platform using SurveyStack's hardware integration

Learn more about SurveyStack's hardware integration [here](https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/getting_started/)

### Required Equipment
Included with the Kit:

- CO2 measurement assembly
- 300 cc modified syringes with valves
- Incubator
- 15 cc scoop
- Cassettes to hold the syringes
- Calibration weights
- Room temperature sensor

Not included with kit:

- Respiration cups (1 oz plastic portion cups), a few are included with the kit
- DI water (kept in the incubator for 24 hours)



### Protocol
The Soil Respiration measurement occurs in three phases:

1. **Set up:** Warm up sensors, calibrate the scale and set up incubation temperature.
2. **Baseline Measurement:** Weigh out soil, add water, take baseline CO2 measurement.
3. **Final measurement:** Measure CO2 accumulation in the soil vessel and calculating the final value (ug C / g soil)

#### Before starting
1. Make sure that you have a Soil Respiration survey for your project. A template for the Soil Respiration project is available (`SAVR standard template`) in the SurveyStack [Question Set Library](https://our-sci.gitlab.io/software/surveystack_tutorials/QSL/). 
2. **Warm up CO2 Sensor:** The CO2 sensor should be turned on at least 15 minutes before starting the CO2 measurements to warm up the sensor. The CO2 values in the first 15 minutes may not be accurate if not warmed up.
3. **Scale calibration:** Make sure the scale is clean and dry. The survey is designed to calibrate with either standard 30g weight or it allows you to use custom weight for the calibration.
4. **Incubator temperature:** It is always set at 22 degree celsius (this can be changed based on teh protocols being used). The incubator can maintain temperatures that are +/- 10 degreees of the room temperature. Follow the following instruction to control the temperature in the incubator.


- **STC 1000 Temperature Controller:**

     - **Turning off:** Press and hold the power button until the controller is turned off (takes up
     to 5 seconds).
     - **Turning on:** Press the power button until the controller is turned on.

- The STC 1000 temperature has four functions:
 
     - **F1 sets the temperature:** Press `S` and hold. It will show either F1/F2 /F3/ F4. Press `up` 
     or `down` arrow to get to F1. When it shows F1 press `S` and hold with one Thumb and use another
     hand to set your required temperature by pressing `up` or `down` arrow. When you reach your 
     required temperature press the power button to lock it. Press the ‘up’ arrow to check if the set 
     temperature displays.
     - **F2 sets temperature difference:** We set our temperature difference at 0.5 degree celsius. So 
     that the temperature fluctuates either 0.5 below or above 22 degree celsius. For this, you press 
     `S` and go to F2 with `up` or `down` arrow. When at F2 press `S` and hold with one hand, use 
     another hand to press `up` or `down` arrow to  make it 0.5.  Press the power button to lock it.
     Press the `down` arrow to check if it displays the temperature difference.
     - **F3 sets the compression delay time:** It is not good to turn on or off the compressor too 
     quickly. So F3 helps to provide enough time for the compressor to start while there are power 
     outage issues. It is set at 3 minutes.
     - **F4 is to calibrate:** Once it is calibrated, we never have to re-calibrate it. So we will not 
     be using this F4 function.  
     
**NOTE:** Here is a [youtube video instruction to set up the temperature.](https://www.youtube.com/watch?v=TQjicdtDVrQ&t=216s)

#### Baseline CO2 Measurement
1. Open up your SurveyStack survey for soil respiration.
2. Enter the following information into the survey:

    - Batch ID (if needed)
    - Stage of measurement (baseline or final)
    - Incubator temperature
    - Room temperature

3. Advance in the survey to the first sample measurment. For each sample, complete the following steps:

- **Empty cup weight:** Take measurement of the empty cup.
- **Fill the soil cup:** Measure dried soil with the 15 cc volume scoop, making sure to 
homogenize the sample before filling the scoop. Overfill the scoop and level the surface with a 
striker or smooth edge tool such as a glass rod. Pour the sample into the soil cup gently without 
spilling. Shake gently to level the surface.
- **Cup weight after adding soil:**  Measure the soil weight by following the prompts in the survey. 

![prep soil](https://gitlab.com/our-sci/manufacturing/respiration-meter-tutorials/-/raw/master/images/prep_soil.png)

- **Water addition:** Follow the prompts in the survey to add water to the sample. The survey will automatically calculate the correct water addition based on the weight and volume of the soil, with the goal of reaching 55% water-filled pore space. The instrument is programmed to pump water itself. 

     - Use DI water that has been in the incubator for 24 hours. 
     - When pumping is complete,  the survey  will give you the actual volume of water added Generally, the actual water addition is within +/- 0.5g from the target water addition. If the water addition is below or over this +/-0.5g value take a new cup and restart from the ‘empty cup weight’ step. This does not happen often. [IS THERE AN ERROR IF THIS HAPPENS] 
     
- **CO2 measurement:** 

     - After water is added, place the cup onto the piston (make sure the piston is loaded into the cassette) and then put the syringe tube over the piston. Be very careful not to spill soil or water at this phase as you need to apply a gentle pressure to insert the syringe over the piston. Try to do this process behind the “salad bar” or transparent board to avoid breathing close to the syringe. 
     - Keep pressing down while pressing the valve tip until you have 210cc headspace. 
     - Place it under the sensor to take the CO2 measurement. The instrument will press 30cc of air from syringe into the measurement chamber, and 180cc of headspace should remain after the measurement.

![prep syringe](https://gitlab.com/our-sci/manufacturing/respiration-meter-tutorials/-/raw/master/images/prep_syringe.png)


4. When all the 36 measurements are complete, click `Get Summary` to output a summary of all sample data in the survey. 
5. Put all the syringes in the incubator for 24 hours (standard protocol) or for the length of incubation called for by another measurement SOP. 

![incubator](https://gitlab.com/our-sci/manufacturing/respiration-meter-tutorials/-/raw/master/images/incubator.png)

#### Final CO2 Measurement
Once the 24 hour incubation (standard protocol), or alternative SOP incubation period, is complete, remove the samples from the incubator and prepare to run final measurements.  

1. Make sure that the CO2 sensor is warmed up. 
2. Open up your SurveyStack survey for soil respiration.
3. Enter the following information into the survey:

    - Batch ID (if needed)
    - Stage of measurement (baseline or final)
    - Incubator temperature
    - Room temperature

4. Advance in the survey to the first sample measurment. Take CO2 measurements for each sample in the
batch. The sensor will press the piston by 50 cc volume and will output CO2 values. 
5. When all the samples in the batch are complete, click `Calculate Final Values` to calculate final 
CO2-C evolved from the soil.
6. Clean all the tubes with a sponge brush after finishing the final survey and keep them dry until
needed for another run.

**NOTE:** the final calculations will adjust the final concentrations based on the actual time difference between the baseline and final measurements (ex: multiply the output by (23.9/24)  for incubations slighlty shorter than 24 hours).
