# Design Process
While designing a new soil respiration sensor there are numerous considerations that need to be addressed.

### Ensuring consistent sample sizes and water addition
We wanted to make sure that using the SAVR kit was simple and user friendly. To that end, we decided to use a constant volume of soil rather than a constant mass for samples. Using a coffee scoop (15 cc) to measure a consistent volume, it was then possible to make the weighing portion of the process run mechanically, dumping the sample into a boat on the scale, assessing volume and weight. Next, a SurveyStack script calculates how much water to add, and automatically pumps the correct amount into the sample cup. See the [repeatability testing](https://our-sci.gitlab.io/manufacturing/respiration-meter-tutorials/validation-testing/#repeatability-testing) section for more validation data on the soil weight and water addition. 

![mass and water](https://gitlab.com/our-sci/manufacturing/respiration-meter-tutorials/-/raw/master/images/integrated_mass_and_water.png)

### Taking accurate CO2 readings
To streamline the next segment of the process, which requires timed steps of taking a background measurement, the sample measurement, and then a background measurement again, we evaluated sensors that could push and pull air through a small space. After cracking open a few ‘black box’ sensors, we developed a prototype box with a fan that could pull air into a small space, let that air diffuse through a membrane, then push the air out in preparation for the next sample. This solved several problems: It provided a way to flush the contents rapidly between each sample, and the air that was being measured only had to diffuse a small distance. 

Once we had identified the correct sensor and fan set up, we tested it to identify the shortest measurement length where the CO2 concentration in the measurement chamber was within 1-2% of CO2 concentration in the incubation vessel. We prepared CO2 standards at medium (~4000 ppm) and high (13,000 – 14,000 ppm) concentrations and measured them at 6-second intervals. The air within the measurement chamber was within 99% of final concentration after 42 to 48 seconds (Fig. 1, right and left).

Based on these findings we developed a standard CO2 measurement protocol for the “final” (post-incubation) CO2 measurement (Fig. 1 bottom). The software selects the maximum CO2 measurement from the final 3 measurements (36, 42, or 48 seconds). After 48 seconds, a fan is used to purge the measurement chamber and bring the internal CO2 level back to ambient levels. The standard measurement protocol includes running the fan for 24 seconds, taking the final measurement time to 74 seconds. The measurement protocol can be changed such that the purge continues until the internal CO2 concentration falls below a prescribed level.

![timing](https://gitlab.com/our-sci/manufacturing/respiration-meter-tutorials/-/raw/master/images/timing_of_measurement.png?ref_type=heads)
*Figure 1. CO2 measurements over time (left), has a percent of final concentration (right) and an example of the standard CO2 measurement protocol (bottom)*

### Increasing Reliability
To increase reliability, we also wanted a bigger sample--the smaller a sample is, the higher variability can be. One chunk of rock or plant matter can dramatically change what’s going on in the soil. Therefore, we designed the instrument to use 300 ml syringes. Upon repeated testing with soils, we learned that we could increase the average mass of soils from 5 g, which early versions of the soil respiration were using, to 15 g with the 300 ml syringes.

## From prototype to product
After working through the process of developing a functional, effective design, the next step was to make it as simple, affordable, and replicable as possible. Design factors to keep in mind included:

- **Use available parts.** Wherever possible, use parts that are already available. For example, the kit’s incubator is fabricated out of a low-cost [insulated food carrier](https://www.amazon.com/Cambro-EPP400110-Insulated-Carrier-Black/dp/B01M9K42SS/ref=sr_1_4?crid=RRZI6CO1941G&keywords=cambro%2Bcooler&qid=1689779566&s=industrial&sprefix=cambro%2Bcool%2Cindustrial%2C110&sr=1-4&ufe=app_do%3Aamzn1.fos.f5122f16-c3e8-4386-bf32-63e904010ad0&th=1) with heating and cooling elements added so that the soils could be incubated at temperatures between from 20 C to 35 C, depending on the measurement protocol, to ensure consistent incubation conditions. 
- **Keep custom parts simple.**	For components that do need to be custom manufactured, the guiding principles are that they be uniform, quick to make, and all the same thickness so they can be cut from a single sheet.

The final steps involved tweaking the design for user-friendliness within the lab process. To simplify sample loading, we added a funnel and gasket attached to a tube and pump to add the right amount of water, and arranged his parts so they moved the shortest distance possible to minimize spills. To keep everything aligned, we added a gear system based on Geneva gears, with cassettes that can hold 7-8 syringes each. The cassette length was based on the depth of the incubator box, so that it is easy to slide cassettes of syringes into and out of the incubator between the baseline and final measurements (Fig. 2).

![incubation](https://gitlab.com/our-sci/manufacturing/respiration-meter-tutorials/-/raw/master/images/incubation.png)
*Figure 2. Geneva gears keep syringes aligned with the sensor (Left). Cassettes fit into the incubator (Right)*

The device includes a plastic shield to prevent respiration from those using the device from interfering with the CO2 measurements in the sensor box. 

![views](https://gitlab.com/our-sci/manufacturing/respiration-meter-tutorials/-/raw/master/images/top_side_views.png)
*Figure 3. Side and top views of the SAVR Kit.*
